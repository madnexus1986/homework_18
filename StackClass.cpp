#include <iostream>

template <typename SampleT>
class  Stack
{
private:
    int size; //������ �����
    int Top;  //������� �����
    SampleT* stackPointer = nullptr;

public:
    Stack(int maxSize) : size(maxSize)
    {
        stackPointer = new SampleT[size];
        Top = 0;
    }
    bool IsFull() //��������, ��� ���� ��������
    {
        return stackPointer[Top] == stackPointer[size];
    }
    bool IsEmpty() //��������, ��� ���� ������
    {
        return Top == 0;
    }
    ~Stack() //���������� �����
    {
        delete[] stackPointer;
        stackPointer = nullptr;
    }
    void push(SampleT pValue) //�������� ������� � ���� 
    {
        if (IsFull())
        {
            std::cout << "Stack is full\n";
        }
        else
        {
            stackPointer[Top++] = pValue;
        }

    }

    void printStack() //������ ���� ��������� �����
    {
        std::cout << "Printing current stack elements " << '\n';
        for (int i = 0; i < Top; i++)
        {
            std::cout << stackPointer[i] << '\n';
        }
        std::cout << "Top element position now is " << Top << '\n';

    }

    void pop() //�������� �������� �������� �� �����
    {
        if (IsEmpty())
        {
            std::cout << "Stack is empty\n";
        }
        else
        {
            stackPointer[--Top];
        }

    }

    void peek() //���������� �������� �������� �������� �����
    {
        if (IsEmpty())
        {
            std::cout << "Stack is empty\n";
        }
        else
        {
            std::cout << "Current top element is " << stackPointer[Top - 1] << '\n';
            std::cout << '\n';
        }
    }

};
