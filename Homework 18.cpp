// Homework 18.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "StackClass.cpp"



int main()
{
    //���� ��� ����� ���� int
    int count = 0;
    int sElem;  //������� ����� ��� ����� ������
    int x; //������ ����� 
    std::cout << "Enter stack size\n";
    std::cin >> x;
    Stack<int> *MainStack = new Stack<int>(x);
    std::cout << "Enter stack elements\n";
    while (count++ < x) // �������� �������� � ����
    {
        std::cin >> sElem;
        MainStack->push(sElem); 
    }
    MainStack->printStack();
    MainStack->peek();
    std::cout << "Popping top element from stack... \n";
    MainStack->pop();
    MainStack->peek();
   MainStack->printStack();
   MainStack->~Stack(); //������� ������ ����� �������
   
   /*
   ������� ����� �������, ���� �� ������������ ����� ������.
   delete MainStack; 
   MainStack = nullptr;
   */
}


